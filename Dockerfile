FROM jenkins/jenkins:2.297

MAINTAINER lenovodevops.com

USER root

RUN mkdir -p /data/repo

RUN mkdir -p /opt/maven

ADD apache-maven-3.8.1 /opt/maven

ENV PATH=/opt/maven/bin:$PATH

USER root